function calcPlano(){
	var minuto = document.getElementById("minutotempo").value;

	var valorinit = 30;
	//valor min excedente
	var minvalorp = 2.09;

	var zero = 0;

	var zerocusto = zero.toFixed(2);


	var resp = (minuto - valorinit) * minvalorp;

	var total = resp.toFixed(2);

	if (minuto == "" || minuto < 0){
		alert("O campo minuto deve ser preenchido e não pode ser negativo");
		document.getElementById("minutotempo").focus();
		return false;
	}

	else if(minuto >= 0 && minuto <=30){
		document.getElementById("barato").innerHTML = zerocusto;
	}else{
		document.getElementById("barato").innerHTML = total;
	}

	calcSemPlano();

	//document.getElementById("barato").innerHTML = total;


}

function calcPlano1(){
	var minuto = document.getElementById("minutotempo1").value;

	var valorinit = 60;
	//valor min excedente
	var minvalorp = 2.09;

	var zero = 0;

	var zerocusto = zero.toFixed(2);


	var resp = (minuto - valorinit) * minvalorp;

	var total = resp.toFixed(2);

	if (minuto == "" || minuto < 0){
		alert("O campo minuto deve ser preenchido e não pode ser negativo");
		document.getElementById("minutotempo1").focus();
		return false;
	}

	else if(minuto >= 0 && minuto <=60){
		document.getElementById("barato1").innerHTML = zerocusto;
	}else{
		document.getElementById("barato1").innerHTML = total;
	}

	calcSemPlano1();

}


function calcPlano2(){
	var minuto = document.getElementById("minutotempo2").value;

	var valorinit = 120;
	//valor min excedente
	var minvalorp = 2.09;

	var zero = 0;

	var zerocusto = zero.toFixed(2);


	var resp = (minuto - valorinit) * minvalorp;

	var total = resp.toFixed(2);

	if (minuto == "" || minuto < 0){
		alert("O campo minuto deve ser preenchido e não pode ser negativo");
		document.getElementById("minutotempo2").focus();
		return false;
	}

	else if(minuto >= 0 && minuto <=120){
		document.getElementById("barato2").innerHTML = zerocusto;
	}else{
		document.getElementById("barato2").innerHTML = total;
	}

	calcSemPlano2();

}





function calcSemPlano(){

	//Pegar valor minuto
	var min = document.getElementById("minutotempo").value;

	// valor minuto sem plano
	var minvalor = 1.90;

	//formula
	var res = minvalor * min;

	var resultado = res.toFixed(2);

	if (min == "") {
		alert("Por favor informe valor do minuto");
		document.getElementById("minutotempo").focus();
		return false;
	}
	//Pegar valor Sem plano

   document.getElementById('caro').innerHTML = resultado;

}

function calcSemPlano1(){

	//Pegar valor minuto
	var min = document.getElementById("minutotempo1").value;

	// valor minuto sem plano
	var minvalor = 1.90;

	//formula
	var res = minvalor * min;

	var resultado = res.toFixed(2);

	if (min == "") {
		alert("Por favor informe valor do minuto");
		document.getElementById("minutotempo1").focus();
		return false;
	}
	//Pegar valor Sem plano

   document.getElementById('caro1').innerHTML = resultado;

}

function calcSemPlano2(){

	//Pegar valor minuto
	var min = document.getElementById("minutotempo2").value;

	// valor minuto sem plano
	var minvalor = 1.90;

	//formula
	var res = minvalor * min;

	var resultado = res.toFixed(2);

	if (min == "") {
		alert("Por favor informe valor do minuto");
		document.getElementById("minutotempo2").focus();
		return false;
	}
	//Pegar valor Sem plano

   document.getElementById('caro2').innerHTML = resultado;

}






var dadosArray = new Array();

	dadosArray[0] = new Array("");
	dadosArray[1] = new Array("011 - São Paulo","017 - Mirassol");



	function change(combo1){
		var valorcombo1 = combo1.value;
		document.forms["formulario"].elements["combo2"].options.length=0;

		for(var i = 0; i < dadosArray[valorcombo1].length; i++){

			var option = document.createElement('option');
			option.setAttribute("value",i+1);
			option.innerHTML = dadosArray[valorcombo1][i];
			document.forms["formulario"].elements["combo2"].appendChild(option);
		}
}

	function change1(combo3){
		var valorcombo1 = combo3.value;
		document.forms["formulario1"].elements["combo4"].options.length=0;

		for(var i = 0; i < dadosArray[valorcombo1].length; i++){

			var option = document.createElement('option');
			option.setAttribute("value",i+1);
			option.innerHTML = dadosArray[valorcombo1][i];
			document.forms["formulario1"].elements["combo4"].appendChild(option);
		}

	}

	function change2(combo5){
		var valorcombo1 = combo5.value;
		document.forms["formulario2"].elements["combo6"].options.length=0;

		for(var i = 0; i < dadosArray[valorcombo1].length; i++){

			var option = document.createElement('option');
			option.setAttribute("value",i+1);
			option.innerHTML = dadosArray[valorcombo1][i];
			document.forms["formulario2"].elements["combo6"].appendChild(option);
		}

	}
function calcula(){

	var estado = document.getElementById("combo1").value == "0";
	
	var validcombo1 = document.getElementById("combo2").value == "1";
	var validcombo2 = document.getElementById("combo2").value == "2";
	//var validcombo3 = document.getElementById("combo2").value == "3";
		//var validcombo2 = combo1.value == "2";
		//var validcombo3 = combo1.value == "3";
	var spinicio = document.getElementById("combo1").value == "1";	

		if(estado){
			alert('A Origem deve ser informada');
			document.getElementById("combo1").focus();
			return false;

		}

		else if(spinicio == validcombo1){
			calcPlano();
		}

		else if(spinicio == validcombo2){
			alert('Entrada inválida - Escolha outro Destino');
			document.getElementById("combo2").focus();

			document.getElementById("barato").innerHTML = "";
			document.getElementById("caro").innerHTML = "";

			return false;
		}
	
}

function calcula1(){

	var estado = document.getElementById("combo3").value == "0";
	
	var validcombo1 = document.getElementById("combo4").value == "1";
	var validcombo2 = document.getElementById("combo4").value == "2";
	

	var spinicio = document.getElementById("combo3").value == "1";	

		if(estado){
			alert('A Origem deve ser informada');
			document.getElementById("combo3").focus();
			return false;

		}

		else if(spinicio == validcombo1){
			calcPlano1();
		}

		else if(spinicio == validcombo2){
			alert('Entrada inválida - Escolha outro Destino');
			document.getElementById("combo4").focus();

			document.getElementById("barato1").innerHTML = "";
			document.getElementById("caro1").innerHTML = "";

			return false;
		}
	
}	

function calcula2(){

	var estado = document.getElementById("combo5").value == "0";
	
	var validcombo1 = document.getElementById("combo6").value == "1";
	var validcombo2 = document.getElementById("combo6").value == "2";
	

	var spinicio = document.getElementById("combo5").value == "1";	

		if(estado){
			alert('A Origem deve ser informada');
			document.getElementById("combo5").focus();
			return false;

		}

		else if(spinicio == validcombo1){
			calcPlano2();
		}

		else if(spinicio == validcombo2){
			alert('Entrada inválida - Escolha outro Destino');
			document.getElementById("combo6").focus();

			document.getElementById("barato2").innerHTML = "";
			document.getElementById("caro2").innerHTML = "";

			return false;
		}
	
}